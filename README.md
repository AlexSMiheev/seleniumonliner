Для запуска тестов используется команда - clean test -Dsuite=TestSuite

Так же предусмотрена возможность прохождения тестов в различных броузерах:

-Dbrowser=ie - Запуск в Internet Explorere

-Dbrowser=firefox - Запуск в Firefox

-Dbrowser=chrome - Запуск в Chrome
 
 Пример: clean test -Dsuite=TestSuite -Dbrowser=chrome