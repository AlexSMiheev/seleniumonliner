package forms;

import framework.UiElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePage {

    private WebElement enterBtn = UiElement.waitForPresentAndGetWebElement(By.className("auth-bar__item--text"));
    private WebElement topLogo = UiElement.waitForPresentAndGetWebElement(By.xpath("//*[contains(@class,'b-top-logo')]//a"));

    public void clickEnterButton() {
        enterBtn.click();
    }

    public String getUrlFromSite() {
        return topLogo.getAttribute("href");
    }

    public boolean enterButtonIsDisplayed() {
        if (enterBtn.isDisplayed()) {
            System.out.println("Logoff is correct");
            return enterBtn.isDisplayed();
        } else {
            System.out.println("log off not executed correctly.");
            return false;
        }
    }
}
