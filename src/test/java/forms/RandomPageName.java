package forms;

import framework.UiElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class RandomPageName {

    private WebElement randomPageName = UiElement.waitForPresentAndGetWebElement(By.tagName("h1"));

    public String getRandomPageName() {
        return randomPageName.getText();
    }
}
