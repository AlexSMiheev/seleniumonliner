package forms;

import framework.UiElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class UserProfileMenu {

    private WebElement exitBtn = UiElement.waitForPresentAndGetWebElement(By.className("b-top-profile__link_secondary"));

    public void clickExitBtn() {
        exitBtn.click();
    }
}
