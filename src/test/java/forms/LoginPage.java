package forms;

import framework.Browser;
import framework.UiElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage {

    private WebElement loginFormTitle = UiElement.waitForPresentAndGetWebElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/div[1]"));
    private WebElement textFildLogin = UiElement.waitForPresentAndGetWebElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[1]/div/div[2]/div/div/div/div/input"));
    private WebElement textFildPassword = UiElement.waitForPresentAndGetWebElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[2]/div/div/div/div/input"));
    private WebElement enterBtn = UiElement.waitForPresentAndGetWebElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[3]/button"));

    public String getLoginFormTitleText() {
        return loginFormTitle.getText();
    }

    public void fillLoginField(String login) {
        System.out.println("Sending login: " + login);
        textFildLogin.sendKeys(login);
    }

    public void fillPasswordFild(String password) {
        System.out.println("Sending password: " + password.replaceAll("([a-zA-Z0-9])", "*"));
        textFildPassword.sendKeys(password);
    }

    public void clickEnterButton() {
        enterBtn.click();
    }

    public boolean isErrorMessageDisplayed() {
        return Browser.getInstance().findElements(By.xpath("//*[contains(@class,'auth-form__description_error')]")).size() > 0;
    }
}
