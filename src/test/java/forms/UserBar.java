package forms;

import framework.UiElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class UserBar {

    private WebElement userBarBtn = UiElement.waitForPresentAndGetWebElement(By.cssSelector("#userbar > div.b-top-profile__list > div.b-top-profile__item.b-top-profile__item_arrow > a"));

    public void clickUserBarBtn() {
        userBarBtn.click();
    }
}
