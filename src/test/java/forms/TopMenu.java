package forms;

import framework.Browser;
import framework.UiElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class TopMenu {

    private static WebElement topMenu = UiElement.waitForPresentAndGetWebElement(By.cssSelector("#container > div > div > div > div > div.project-navigation.project-navigation_overflow.project-navigation_scroll > div > div.project-navigation__part.project-navigation__part_1 > ul"));
    private List<WebElement> topMenuElements = Browser.getInstance().findElements(By.xpath("//ul[contains(@class,'project-navigation__list_secondary')]//span[contains(@class,'project-navigation__sign')]"));

    public String clickRandomItemAndGetName() {
        if (topMenu.isDisplayed()) {
            for (int i = 0; i < topMenuElements.size(); i++) {
                System.out.println("Element: " + i + " name: " + topMenuElements.get(i).getText());
            }
            WebElement menuItem = topMenuElements.get((int) (Math.random() * topMenuElements.size()));
            String selectedItem = menuItem.getText();
            System.out.println("Click to random menu item: " + selectedItem);
            menuItem.click();
            return selectedItem;
        } else {
            System.out.println("TOP MENU NOT FOUND");
            return null;
        }
    }
}
