package framework.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    public static String getBrowserNameFromProperty() {
        return getProperties().getProperty("defaultBrowser");
    }

    public static String getStartUrl() {
        return getProperties().getProperty("startUrl");
    }

    public static int getMaximumWaitForElementToLoad() {
        return Integer.valueOf(getProperties().getProperty("maximumWaitForElementToLoad"));
    }

    public static Properties getProperties() {
        Properties prop = new Properties();
        try (InputStream input = PropertiesReader.class.getClassLoader().getResourceAsStream("project.properties")) {
            if (input == null) {
                System.out.println("Sorry, unable to find project.properties");
            } else {
                prop.load(input);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
