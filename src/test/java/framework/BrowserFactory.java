package framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.io.File;
import java.net.URISyntaxException;

public class BrowserFactory {

    private static File myFile = null;

    private static WebDriver getChromeDriver() {
        try {
            myFile = new File(ClassLoader.getSystemResource("drivers/chromedriver.exe").toURI());
        } catch (URISyntaxException e) {
            System.err.println("Could not find driver file for Chrome Browser" + e.getMessage());
        }
        System.setProperty("webdriver.chrome.driver", myFile.getAbsolutePath());
        return new ChromeDriver();
    }

    private static WebDriver getFirefoxDriver() {
        try {
            myFile = new File(ClassLoader.getSystemResource("drivers/geckodriver.exe").toURI());
        } catch (URISyntaxException e) {
            System.err.println("Could not find driver file for Firefox Browser" + e.getMessage());
        }
        System.setProperty("webdriver.gecko.driver", myFile.getAbsolutePath());
        return new FirefoxDriver();
    }

    private static WebDriver getIEDriver() {
        try {
            myFile = new File(ClassLoader.getSystemResource("drivers/IEDriverServer.exe").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
            System.err.println("Could not find driver file for IExplorer Browser" + e.getMessage());
        }
        System.setProperty("webdriver.ie.driver", myFile.getAbsolutePath());
        return new InternetExplorerDriver();
    }

    public static WebDriver getDriver(String typeOfDriver) {
        switch (typeOfDriver) {
            case "chrome":
                return getChromeDriver();
            case "firefox":
                return getFirefoxDriver();
            case "ie":
                return getIEDriver();
            default:
                return null;
        }
    }
}