package framework;

import framework.utils.PropertiesReader;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UiElement {

    public static WebElement waitForPresentAndGetWebElement(By locator) {
        return new WebDriverWait(Browser.getInstance(), PropertiesReader.getMaximumWaitForElementToLoad()).ignoring(StaleElementReferenceException.class).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
