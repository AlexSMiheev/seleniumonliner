package framework;

import framework.utils.PropertiesReader;
import org.openqa.selenium.WebDriver;

public class Browser {

    private static WebDriver driver;

    private Browser() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            driver = BrowserFactory.getDriver(System.getProperty("browser", PropertiesReader.getBrowserNameFromProperty()));
        }
        return driver;
    }
}
