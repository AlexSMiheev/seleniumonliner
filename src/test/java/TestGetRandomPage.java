import forms.*;
import framework.Browser;
import framework.utils.PropertiesReader;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class TestGetRandomPage {


    @BeforeTest
    public void start() {
        Browser.getInstance().manage().window().maximize();
    }

    @Parameters({"login", "password"})
    @Test
    public void getRandomPage(String login, String password) {

        System.out.println("Step 1: Proceeding to - " + PropertiesReader.getStartUrl());
        Browser.getInstance().get(PropertiesReader.getStartUrl());
        HomePage homePage = new HomePage();
        System.out.println("Compare the requested page " + homePage.getUrlFromSite() + " with received page " + PropertiesReader.getStartUrl());
        assertEquals(homePage.getUrlFromSite(), PropertiesReader.getStartUrl(), "Open page is incorrect!!!");

        System.out.println("Step 2: Performing authorization");
        homePage.clickEnterButton();
        LoginPage loginPage = new LoginPage();
        assertEquals("Вход", loginPage.getLoginFormTitleText());
        loginPage.fillLoginField(login);
        loginPage.fillPasswordFild(password);
        loginPage.clickEnterButton();
        assertTrue(!loginPage.isErrorMessageDisplayed(), "Invalid login or password!!!");

        System.out.println("Step 3: Click random catalog menu item");
        TopMenu topMenu = new TopMenu();
        String randomMenuName = topMenu.clickRandomItemAndGetName();
        RandomPageName randomPageName = new RandomPageName();
        System.out.println("Random page name: " + randomMenuName + " compare with " + randomPageName.getRandomPageName());
        assertEquals(randomMenuName, randomPageName.getRandomPageName());

        System.out.println("Step 4: Performing log out");
        UserBar userBar = new UserBar();
        userBar.clickUserBarBtn();
        UserProfileMenu userProfileMenu = new UserProfileMenu();
        userProfileMenu.clickExitBtn();
        homePage = new HomePage();

        assertTrue(homePage.enterButtonIsDisplayed());

        System.out.println("Test finished at: " + LocalDateTime.now());
    }

    @AfterTest
    public void stop() {
        Browser.getInstance().quit();
        System.out.println("Browser is closed.");
    }
}
